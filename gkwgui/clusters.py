import tkinter as tk
import tkinter.ttk as ttk

class ClustersFrame(ttk.Frame):

    label = 'Clusters'
    
    def __init__(self, settings, master=None):
        super().__init__()
        self.settings = settings

        self.buttonsframe = tk.Frame(self)
        self.button = ttk.Button(self.buttonsframe, text='refresh graphs', command=self.refresh_graphs)
        self.button.pack(side='left')
        self.buttonsframe.grid(column=0,row=0,sticky=tk.W)
        

        self.graph_labels = []
        self.row_offset = 1

        self.refresh_graphs()

    def refresh_graphs(self):
        import os
        import PIL.Image
        import PIL.ImageTk
        # get the full path to this Python file here
        dir_path = os.path.dirname(os.path.realpath(__file__))
        graphs_path = os.path.join(dir_path,'../graphs')
        try:
            graphs = os.listdir(graphs_path)
            try:
                graphs.remove('jobs_history.png')
            except ValueError:
                # that file does not exist. Ignore this.
                pass

            for i,f in enumerate(graphs):
                if(not f.endswith('png')):
                    continue
                abspath = os.path.join(graphs_path,f)
                # use the Pillow library to read a png image (only very
                # recent Tkinter supports png)
                graphImage = PIL.ImageTk.PhotoImage(PIL.Image.open(abspath))
                if(len(self.graph_labels) == i):
                    self.graph_labels.append(ttk.Label(self, image=graphImage))
                    self.graph_labels[i].grid(column=i % 2, row=self.row_offset+int(i / 2), sticky= i%2==0 and tk.E or tk.W)
                    self.graph_labels[i].image = graphImage  # keep a reference!
                else:
                    self.graph_labels[i]['image'] = graphImage
                    self.graph_labels[i].image = graphImage  # keep a reference!

            for i in range(len(graphs), len(self.graph_labels)):
                self.graph_labels[i].grid_forget()
        except Exception as err:
            print("Could not load PNG image files from %s" % graphs_path)
            print(err)
