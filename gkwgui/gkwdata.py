import tkinter as tk
import tkinter.ttk as ttk
import os

class GKWDataFrame(ttk.Frame):

    label = 'GKW-Data'

    def __init__(self, master, settings, remoteshells):
        super().__init__()

        self.settings = settings

        self.remoteshells = remoteshells

        self.buttonsframe = tk.Frame(self)
        self.button = ttk.Button(self.buttonsframe, text='refresh tree', command=self.rebuild_tree)
        self.button.pack(side='left')
        self.button = ttk.Button(self.buttonsframe, text='update information', command=self.update_information)
        self.button.pack(side='left')
        # self.button = ttk.Button(self.buttonsframe, text='update full sync information', command=self.update_sync_information)
        # self.button.pack(side='left')

        
        self.progressbar_value = tk.DoubleVar()
        self.progressbar = ttk.Progressbar(self.buttonsframe, variable=self.progressbar_value, mode='determinate',
                                           orient=tk.HORIZONTAL, maximum=100)
        self.progressbar.pack(side='left')

        self.message_label = tk.Label(self.buttonsframe, text="")
        self.message_label.pack(side='left')
        self.buttonsframe.grid(column=0,row=0,sticky=tk.W)
        
        ####################################################################################################
        self.tree = ttk.Treeview(self)

        colheadings = [folder for folder in self.settings["folders"]]
        colheadings += [cluster for cluster in self.settings["clusters"]]
        colnames = [colheading+"col" for colheading in colheadings]
        self.tree["columns"] = colnames
        for colheading in colheadings:
            colname = colheading + "col"
            self.tree.column(colname, width=20)
            self.tree.heading(colname, text=colheading)
        
        # make that cell of the grid stretchable
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        self.tree.grid(column=0,row=1,sticky=tk.N+tk.S+tk.E+tk.W)
        scrollbar = tk.Scrollbar(self)
        scrollbar.grid(column=1, row=1, sticky=tk.N+tk.S+tk.E+tk.W)
        scrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=scrollbar.set)

        ####################################################################################################
        
        self.details_frame = tk.LabelFrame(self, text="Programs & Tools")
        diff_select_frame = tk.Frame(self.details_frame)
        
        r = 0

        label = tk.Label(diff_select_frame, text="At location ")
        label.pack(side='left')
        
        self.location_combo_value = tk.StringVar()
        #self.location_combo_value.trace('w',self.update_location_combo)
        location_combo = ttk.Combobox(diff_select_frame, values=colheadings, state="readonly",
                                       textvariable=self.location_combo_value)
        location_combo.current(0)
        location_combo.pack(side='left')

        self.button = ttk.Button(diff_select_frame, text='execute tool: ', command=self.execute_tool)
        self.button.pack(side='left')

        self.tool_combo_value = tk.StringVar()
        #self.tool_combo_value.trace('w',self.update_tool_combo)
        tool_combo_values = [program for program in self.settings['programs']]
        tool_combo = ttk.Combobox(diff_select_frame, values=tool_combo_values,
                                       textvariable=self.tool_combo_value)
        tool_combo.current(0)
        tool_combo.pack(side='left')

        # file_diff_label = tk.Label(diff_select_frame, text="Relative path for two-file diff:")
        # file_diff_label.pack(side='left')
        
        # self.file_diff_entry = ttk.Combobox(diff_select_frame, width=70,
        #                                     values=["input.dat", "simseries.txt", "jobscript", "log.txt"])
        # self.file_diff_entry.bind('<Return>', self.update_two_file_diff)
        # self.file_diff_entry.pack(side='left')
        diff_select_frame.grid(column=0, row=r, sticky=tk.N+tk.W)

        self.details_text = tk.Text(self.details_frame, width=120, relief='groove')
        self.details_text.insert(tk.END, "(nothing to display yet)\n")
        r += 1
        self.details_text.grid(column=0, row=r, columnspan=2, sticky=tk.N+tk.S+tk.E+tk.W)
        # make that cell stretchable
        self.details_frame.columnconfigure(0, weight=1)
        self.details_frame.rowconfigure(r, weight=1)
        scrollbar = tk.Scrollbar(self.details_frame)
        scrollbar.grid(column=2, row=r, sticky=tk.N+tk.S+tk.E+tk.W)
        scrollbar.config(command=self.details_text.yview)
        self.details_text.config(yscrollcommand=scrollbar.set)
        self.details_frame.grid(column=2,row=1,sticky=tk.N+tk.S+tk.E+tk.W)
        
        self.rebuild_tree()
        
    def execute_tool(self):
        import subprocess
        import spur
        tool_combo_value = self.tool_combo_value.get()
        location_combo_value = self.location_combo_value.get()
        items = self.tree.selection()

        text = ""
        try:
            item = items[0]
        except Exception as err:
            text += "Nothing selected."
            
        try:
            command = self.settings['programs'][tool_combo_value]['executable']
            in_shell = self.settings['programs'][tool_combo_value]['shell']
        except:
            command = tool_combo_value
            in_shell = False

        try:
            if(location_combo_value in self.settings['folders']):
                working_dir = os.path.join(self.settings['folders'][location_combo_value]['path'],
                                           self.get_corresponding_path(item))
            else:
                working_dir = os.path.join(self.settings['clusters'][location_combo_value]['home'],
                                           self.settings['calc_folder_name'],
                                           self.get_corresponding_path(item))

            if(location_combo_value in self.settings['folders']):
                p = subprocess.Popen(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True,cwd=working_dir,shell=in_shell)
                output = p.communicate()
                text += output[0] + output[1]
            else:
                remote_shell = spur.SshShell(
                    hostname=self.settings["clusters"][location_combo_value]["hostname"],
                    username=self.settings["clusters"][location_combo_value]["username"],
                    private_key_file=self.settings["clusters"][location_combo_value]["private_key_file"]
                )
                #with self.remoteshells[location_combo_value] as shell:
                with remote_shell as shell:
                    result = shell.run(command, cwd=working_dir,
                                       encoding=self.settings["clusters"][location_combo_value]["output_encoding"])
                    text += result.output
        except Exception as err:
            text += str(err)

        self.update_details_text(text)
    
    def update_details_text(self, text, inc_progressbar=True, selected_item=""):
        self.details_text.delete("1.0", tk.END)
        self.details_text.insert(tk.END, text)
        #self.details_frame.configure(text="Sync/diff details for selection: "+selected_item)
        # if(inc_progressbar):
        #     self.increment_progressbar()

    # def increment_progressbar(self):
    #     #update the progressbar
    #     self.progressbar_value.set(self.progressbar_value.get() + 1.0)
    #     self.update_idletasks()

    def get_corresponding_path(self, item):
        if(len(self.tree.get_children(item)) != 0):
            # this is not a leaf node
            return None
        else:
            # this is a leaf node. find all parents and construct a path
            path = self.tree.item(item,option='text')
            parent_item = self.tree.parent(item)
            while(parent_item != ''):
                path = os.path.join(self.tree.item(parent_item,option='text'), path)
                parent_item = self.tree.parent(parent_item)
            return path
    
    def rebuild_tree_visit_project_folder(self, parent_item, parent_path):
        import re
        run_folder_re = re.compile(r"^[0-9]+(?:\.\w+)?$")

        for f in os.listdir(parent_path):
            fpath = os.path.join(parent_path,f)
            if(os.path.isdir(fpath)):
                if(run_folder_re.match(f)):
                    self.tree.insert(parent_item,"end", text=f)
                else:
                    item = self.tree.insert(parent_item,"end", text=f)
                    self.rebuild_tree_visit_project_folder(item, fpath)
    
    def rebuild_tree(self):
        # remove all
        for item in self.tree.get_children():
            self.tree.delete(item) 

        self.rebuild_tree_visit_project_folder("", self.settings['folders']['setup']['path'])
        

    def update_information(self):

        items = self.tree.selection()
        if(items == ''):
            # update all items
            #items = self.tree.get_children()
            items = []
            self.message_label.configure(text="Select a run from the project tree first!")
        else:
            self.message_label.configure(text="Obtaining information, please wait...")

            for item in items:
                row_values = []
                #selected_item_text = self.tree.item(item,option='text')
                relative_path = self.get_corresponding_path(item)

                for folder in self.settings["folders"]:
                    check_result = "not implemented"
                    #self.check_hdf5(folder["path"])
                    row_values.append(check_result)
                    
                for cluster in self.settings["clusters"]:
                    check_result = "not implemented"
                    row_values.append(check_result)
                
                self.tree.item(item,values = row_values)
            self.message_label.configure(text="")
