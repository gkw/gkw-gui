import tkinter as tk
import tkinter.ttk as ttk
import os

class DataFrame(ttk.Frame):

    label = 'Data Sync'

    TECHNICAL_DIFF = "technical diff"
    DATA_DIFF = "data diff"
    GKW_DATA_DIFF = "gkw data diff"
    TWO_FILE_DIFF = "two file diff"

    not_exist_format = r'change_dir "%s" failed: No such file or directory'
    
    def __init__(self, master, settings, remoteshells):
        super().__init__()

        self.settings = settings

        self.remoteshells = remoteshells

        self.buttonsframe = tk.Frame(self)
        self.button = ttk.Button(self.buttonsframe, text='refresh tree', command=self.rebuild_tree)
        self.button.pack(side='left')
        self.button = ttk.Button(self.buttonsframe, text='update standard sync information', command=self.update_std_sync_information)
        self.button.pack(side='left')
        self.button = ttk.Button(self.buttonsframe, text='update full sync information', command=self.update_sync_information)
        self.button.pack(side='left')

        
        self.progressbar_value = tk.DoubleVar()
        self.progressbar = ttk.Progressbar(self.buttonsframe, variable=self.progressbar_value, mode='determinate',
                                           orient=tk.HORIZONTAL, maximum=100)
        self.progressbar.pack(side='left')

        self.message_label = tk.Label(self.buttonsframe, text="")
        self.message_label.pack(side='left')
        self.buttonsframe.grid(column=0,row=0,sticky=tk.W)
        
        self.tree = ttk.Treeview(self)

        colnames = []
        arrow = ' vs. '
        colheadings = []
        for cluster in self.settings["clusters"]:
            for folder in self.settings["folders"]:
                colname = cluster+"-"+folder+"-col"
                colnames += (colname,)
                
        self.tree["columns"] = colnames
        
        for cluster in self.settings["clusters"]:
            for folder in self.settings["folders"]:
                colname = cluster+"-"+folder+"-col"
                self.tree.column(colname, width=100)
                colheadings.append(cluster+arrow+folder)
                self.tree.heading(colname, text=colheadings[-1])
        
        # make that cell of the grid stretchable
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        self.tree.grid(column=0,row=1,sticky=tk.N+tk.S+tk.E+tk.W)
        scrollbar = tk.Scrollbar(self)
        scrollbar.grid(column=1, row=1, sticky=tk.N+tk.S+tk.E+tk.W)
        scrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=scrollbar.set)
        
        self.details_frame = tk.LabelFrame(self, text="Sync/diff details:")
        diff_select_frame = tk.Frame(self.details_frame)
        
        r = 0
        self.diff_pair_combo_value = tk.StringVar()
        self.diff_pair_combo_value.trace('w',self.update_diff_pair_combo)
        diff_pair_combo = ttk.Combobox(diff_select_frame, values=colheadings, state="readonly",
                                       textvariable=self.diff_pair_combo_value)
        diff_pair_combo.current(0)
        diff_pair_combo.pack(side='left')

        self.diff_type_combo_value = tk.StringVar()
        self.diff_type_combo_value.trace('w',self.update_diff_type_combo)
        diff_type_values = (self.TECHNICAL_DIFF, self.DATA_DIFF, self.GKW_DATA_DIFF, self.TWO_FILE_DIFF)
        diff_type_combo = ttk.Combobox(diff_select_frame, values=diff_type_values, state="readonly",
                                       textvariable=self.diff_type_combo_value)
        diff_type_combo.current(0)
        diff_type_combo.pack(side='left')

        file_diff_label = tk.Label(diff_select_frame, text="Relative path for two-file diff:")
        file_diff_label.pack(side='left')
        
        self.file_diff_entry = ttk.Combobox(diff_select_frame, width=70,
                                            values=["input.dat", "simseries.txt", "jobscript", "log.txt"])
        self.file_diff_entry.bind('<Return>', self.update_two_file_diff)
        self.file_diff_entry.pack(side='left')
        diff_select_frame.grid(column=0, row=r, sticky=tk.N+tk.W)

        self.details_text = tk.Text(self.details_frame, width=120, relief='groove')
        self.details_text.insert(tk.END, "(nothing to display yet)\n")
        r += 1
        self.details_text.grid(column=0, row=r, columnspan=2, sticky=tk.N+tk.S+tk.E+tk.W)
        # make that cell stretchable
        self.details_frame.columnconfigure(0, weight=1)
        self.details_frame.rowconfigure(r, weight=1)
        scrollbar = tk.Scrollbar(self.details_frame)
        scrollbar.grid(column=2, row=r, sticky=tk.N+tk.S+tk.E+tk.W)
        scrollbar.config(command=self.details_text.yview)
        self.details_text.config(yscrollcommand=scrollbar.set)
        self.details_frame.grid(column=0,row=2,columnspan=2,sticky=tk.N+tk.S+tk.E+tk.W)
        
        self.rebuild_tree()

    def update_diff_pair_combo(self, var_obj, b, trace_mode):
        try:
            self.update_std_sync_information()
        except:
            # ignore the error which occurs when the treeview does not yet exist
            pass

    def update_diff_type_combo(self, var_obj, b, trace_mode):
        try:
            self.update_std_sync_information()
        except:
            # ignore the error which occurs when the treeview does not yet exist
            pass

    def update_two_file_diff(self, event=None):
        self.diff_type_combo_value.set(self.TWO_FILE_DIFF)
        diff_type_combo_value = self.diff_type_combo_value.get()
        file_path = self.file_diff_entry.get().strip()
        try:
            item = self.tree.selection()[0]
            selected_item_text = self.tree.item(item,option='text')

            relative_path = self.get_corresponding_path(item)

            local_base, cluster = self.get_selected_for_details()

            local_folder = os.path.join(self.settings['folders'][local_base]['path'],
                                        relative_path)
            remote_folder = os.path.join(self.settings['clusters'][cluster]['home'],
                                         self.settings['calc_folder_name'],
                                         relative_path)
                    
        
            if(len(file_path) > 0):
                import spur
                import difflib
                import pprint
                localshell = spur.LocalShell()
                print("Connecting to %s@%s..." % (self.settings['clusters'][cluster]["username"],self.settings['clusters'][cluster]["hostname"]))
                remoteshell = spur.SshShell(
                    hostname=self.settings['clusters'][cluster]["hostname"],
                    username=self.settings['clusters'][cluster]["username"],
                    private_key_file=self.settings['clusters'][cluster]["private_key_file"]
                )
                remote_file_path = os.path.join(remote_folder, file_path)
                local_file_path = os.path.join(local_folder, file_path)
                print(remote_file_path)
                print(local_file_path)
                with remoteshell.open(remote_file_path, "r") as remote_file, localshell.open(local_file_path, "r") as local_file:
                    d = difflib.Differ()
                    result = list(d.compare(remote_file.read().split('\n'), local_file.read().split('\n')))
                    diff_text = "\n".join(result)
            else:
                diff_text = "(enter a relative path to a file, or copy it from one of the other diff listings)"
        except Exception as err:
            selected_item_text = ""
            print(err)
            diff_text = "(nothing to compare)\n\n" + str(err)
        finally:
            self.update_details_text(inc_progressbar=True,
                                     text=diff_text,
                                     selected_item=selected_item_text)

    def is_selected_for_details(self, local_base, clustername, diff_type=None):
        diff_pair_combo_value = self.diff_pair_combo_value.get()
        diff_type_combo_value = self.diff_type_combo_value.get()
        #print("%s %s %s - %s %s" % (local_base, clustername, diff_type, diff_pair_combo_value, diff_type_combo_value))
        return (diff_pair_combo_value.startswith(clustername) and
                diff_pair_combo_value.endswith(local_base) and
                (diff_type is None and True or (diff_type_combo_value == diff_type)))

    def get_selected_for_details(self):
        for cluster in self.settings["clusters"]:
            for b in self.settings['folders']:
                if(self.is_selected_for_details(b, cluster)):
                    return b, cluster
        
    def update_details_text_push_pull(self, push_stdout, pull_stdout, selected_item=""):
        print("update_details_text:")
        bar = '='*37
        text = (bar+' PUSH '+bar+'\n'+
                push_stdout+'\n\n'+
                bar+' PULL '+bar+'\n'+
                pull_stdout)
        self.update_details_text(text, selected_item=selected_item)

    def update_details_text(self, text, inc_progressbar=True, selected_item=""):
        self.details_text.delete("1.0", tk.END)
        self.details_text.insert(tk.END, text)
        self.details_frame.configure(text="Sync/diff details for selection: "+selected_item)
        if(inc_progressbar):
            self.increment_progressbar()

    def increment_progressbar(self):
        #update the progressbar
        self.progressbar_value.set(self.progressbar_value.get() + 1.0)
        self.update_idletasks()


    def update_std_sync_information(self):
        self.update_sync_information(full_info=False)
        
    def update_sync_information(self, full_info=True):
        import subprocess

        items = self.tree.selection()
        if(items == ''):
            # update all items
            #items = self.tree.get_children()
            items = []
            self.message_label.configure(text="Select a run from the project tree first!")
        else:
            self.message_label.configure(text="Obtaining information, please wait...")

        self.progressbar_value.set(0.0)
        maximum = len(items) * len(self.settings["clusters"]) * 2 + 1
        self.progressbar.configure(maximum=maximum)
        self.update_idletasks()

        rsync_command = {}
        rsync_command[self.TECHNICAL_DIFF] = ['rsync','--itemize-changes']
        #rsync_command[self.DATA_DIFF] = ['rsync', '--rsh=ssh', '--dry-run','--recursive', '--archive','--update','--list-only', '--info=all4']
        rsync_command[self.DATA_DIFF] = ['rsync', '--out-format="%o	%l	%M last mod	%n"', '--dry-run']
        for key in rsync_command.keys():
            for suffix in ['eps','png','jpg','tex','pdf','ps']:
                rsync_command[key].append("--exclude=*.%s" % suffix)
                # ignore common backup files

            #--compress # is probably unnecessary for a dry-run
            #--delete # maybe this allows to get the symmetric diff with just one run
            rsync_command[key] += ['--dry-run', '--rsh=ssh', '--recursive', '--times', '--copy-links','--perms','--update' ]
            rsync_command[key] += ['--exclude=#*#', '--exclude=*~']
            rsync_command[key] += ['--exclude=gkw.x']
            rsync_command[key] += ['--no-group', '--no-owner']


        details_updated = False
        for item in items:
            row_values = []
            selected_item_text = self.tree.item(item,option='text')
            for cluster in self.settings["clusters"]:
                try:
                    for local_base in self.settings["folders"]:

                        relative_path = self.get_corresponding_path(item)
                        local_path = self.settings['folders'][local_base]['path']
                        local_folder = os.path.join(local_path, relative_path)
                        remote_folder = os.path.join(self.settings['clusters'][cluster]['home'],
                                                     self.settings['calc_folder_name'],
                                                     relative_path)
                        remote_target = self.settings['clusters'][cluster]["username"]+'@'+self.settings['clusters'][cluster]["hostname"]+':'+remote_folder
                        def update_special_diff_output(local_base, full_info):
                            diff_type_combo_value = self.diff_type_combo_value.get()
                            if(self.is_selected_for_details(local_base, cluster, diff_type_combo_value)):
                                if(diff_type_combo_value == self.DATA_DIFF):
                                    # execute an extra rsync call and display the results
                                    if(full_info or self.settings['folders'][local_base]['typical_transfer'] == 'push'):
                                        command = rsync_command[diff_type_combo_value] + [local_folder+'/', remote_target]
                                        print("command:" + ' '.join(command))
                                        p = subprocess.Popen(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
                                        push_output = p.communicate()
                                    else:
                                        push_output = ['','']

                                    if(full_info or self.settings['folders'][local_base]['typical_transfer'] == 'pull'):
                                        command = rsync_command[diff_type_combo_value] + [remote_target+'/', local_folder]
                                        p = subprocess.Popen(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
                                        pull_output = p.communicate()
                                    else:
                                        pull_output = ['','']
                                    self.update_details_text_push_pull(push_output[0], pull_output[0], selected_item=selected_item_text)
                                    return True
                                elif(diff_type_combo_value == self.TWO_FILE_DIFF):
                                    self.update_two_file_diff()
                                    return True
                            return False

                        try:
                            if(full_info or self.settings['folders'][local_base]['typical_transfer'] == 'push'):
                                command = rsync_command[self.TECHNICAL_DIFF] + [local_folder+'/', remote_target]
                                print('Comparing folders... ' + ' '.join(command))
                                p = subprocess.Popen(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
                                push_output = p.communicate()
                                if(not full_info and not self.remote_folder_exists(cluster, remote_folder)):
                                    # fake an error message into the stderr string.
                                    # this is then parsed by the parse_rsync_output function!
                                    # This is, because we want to see 'does not exist remotely' also if
                                    # only the push transfer is evaluated. without this extra check, we
                                    # would get "all files need to be updated", i.e. an arrow with the
                                    # number of files.
                                    push_output = (push_output[0], self.not_exist_format % remote_folder)
                                    print("faked push_output:")
                                    print(push_output)
                            else:
                                push_output = ['','']

                            if(full_info or self.settings['folders'][local_base]['typical_transfer'] == 'pull'):
                                command = rsync_command[self.TECHNICAL_DIFF] + [remote_target+'/', local_folder]
                                print('Comparing folders... ' + ' '.join(command))
                                p = subprocess.Popen(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
                                pull_output = p.communicate()
                            else:
                                pull_output = ['','']

                            row_values.append(self.parse_rsync_output(cluster, push_output[0], push_output[1], pull_output[0], pull_output[1], local_path))
                            if(not details_updated and self.is_selected_for_details(local_base, cluster, self.TECHNICAL_DIFF)):
                                details_updated = True
                                self.update_details_text_push_pull(push_output[0], pull_output[0], selected_item=selected_item_text)
                        except Exception as err:
                            row_values.append(str(err))
                        finally:
                            self.increment_progressbar()

                        if(not details_updated):
                            details_updated = update_special_diff_output(local_base, full_info)

                    print(80*'=')
                except Exception as err:
                    print(err)
            self.tree.item(item,values = row_values)

            self.message_label.configure(text="")

        self.update_gkw_data_diff_output(items[0])

    def update_gkw_data_diff_output(self, item):
        diff_type_combo_value = self.diff_type_combo_value.get()
        selected_item_text = self.tree.item(item,option='text')
        if(diff_type_combo_value == self.GKW_DATA_DIFF):
            relative_path = self.get_corresponding_path(item)
            self.update_details_text(self.get_gkw_data_diff(relative_path), selected_item=selected_item_text)
                    
    def get_gkw_data_diff(self, relative_path):
        import subprocess
        import datetime
        import os
        try:
            import pandas as pd
        except:
            return "pandas library is not available. Please install pandas (e.g. with pip3 install --user pandas)"
        try:
            import paramiko
        except:
            return "paramiko library is not available. Please install paramiko (e.g. with pip3 install --user paramiko)"
        
        gkw_files_regex = []
        gkw_files_regex += [r"-regex '.*/%s$'" % pattern for pattern in self.settings["codes"][0]["input_files"]]
        gkw_files_regex += [r"-regex '.*/%s$'" % pattern for pattern in self.settings["codes"][0]["data_files"]]
        gkw_files_regex += [r"-regex '.*/%s$'" % pattern for pattern in self.settings["codes"][0]["scan_files"]]
        gkw_files_regex += [r"-regex '.*/%s$'" % pattern for pattern in self.settings["codes"][0]["tech_files"]]
        gkw_files_regex_args = ' -o '.join(gkw_files_regex)
        format_only_name = r"-printf '%P\n'"
        format_name_mdate_size = r"-printf '%t %s %P\n'"
        
        clients = {}
        files_set = set()
        
        for cluster in self.settings["clusters"]:
            #print("Connecting to %s..." % self.settings['clusters'][cluster]["hostname"])
            clients[cluster].append(paramiko.SSHClient())
            clients[cluster].load_system_host_keys()
            clients[cluster].connect(
                hostname=self.settings['clusters'][cluster]["hostname"],
                username = self.settings['clusters'][cluster]["username"],
                key_filename = self.settings['clusters'][cluster]["private_key_file"],
                look_for_keys = True,
                timeout = 10
            )
            folder = os.path.join(self.settings['clusters'][cluster]['home'],self.settings['calc_folder_name'],relative_path)
            command_find_gkw_data = r"find %s \( %s \) %s " % (folder, gkw_files_regex_args, format_only_name)
            print(command_find_gkw_data)
            stdin, stdout, stderr = clients[cluster].exec_command(command_find_gkw_data)
            # the output of the unix find command is one filename per line
            files_found = [filename.strip() for filename in stdout.readlines()]
            #print("".join(stderr.readlines()))
            files_set.update(files_found)

        for local_folder in [self.settings['folders'][folder]['path'] for folder in self.settings['folders']]:
            folder = os.path.join(local_folder, relative_path)
            command_find_gkw_data = r"find %s \( %s \) %s " % (folder, gkw_files_regex_args, format_only_name)
            p = subprocess.Popen(command_find_gkw_data, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True,shell=True)
            output = p.communicate()
            files_found = [filename.strip() for filename in output[0].split("\n")]
            files_set.update(files_found)

        print("sorting file set:")
        # print(files_set)
        files_set = list(files_set)
        files_set.sort()

        clusternames = [cluster for cluster in self.settings["clusters"]]
        d = pd.DataFrame(index=pd.MultiIndex.from_product([files_set, clusternames + [folder for folder in self.settings['folders']]]),
                         columns=["mtime","size"])

        for clustername in self.settings["clusters"]:
            sftpclient = clients[clustername].get_transport().open_sftp_client()
            for filename in files_set:
                try:
                    absolute_path = os.path.join(self.settings['clusters'][clustername]['home'], self.settings['calc_folder_name'],relative_path,filename)
                    statinfo = sftpclient.stat(absolute_path)
                    mtime = datetime.datetime.fromtimestamp(statinfo.st_mtime).replace(microsecond=0)
                    d.loc[(filename,clustername), "mtime"] = mtime
                    d.loc[(filename,clustername), "size"] = statinfo.st_size
                except FileNotFoundError as err:
                    pass
            sftpclient.close()

        for name in self.settings['folders']:
            folder = self.settings['folders'][name]['path']
            for filename in files_set:
                try:
                    absolute_path = os.path.join(folder,relative_path,filename)
                    statinfo = os.stat(absolute_path)
                    mtime = datetime.datetime.fromtimestamp(statinfo.st_mtime).replace(microsecond=0)
                    d.loc[(filename,name), "mtime"] = mtime
                    d.loc[(filename,name), "size"] = statinfo.st_size
                except FileNotFoundError as err:
                    pass
            
            
        for client in clients:
            client.close()
        # how to treat the nan entries
        d.dropna(subset=['mtime', 'size'], inplace=True)
        
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)
        pd.set_option('display.max_colwidth', 100)
        return d.to_string()

    def remote_folder_exists(self, cluster, absolute_path):
        import paramiko
        ret = True
        try:
            #print("Connecting to %s..." % self.settings['clusters'][cluster]["hostname"])
            client = paramiko.SSHClient()
            client.load_system_host_keys()
            client.connect(
                hostname=self.settings['clusters'][cluster]["hostname"],
                username = self.settings['clusters'][cluster]["username"],
                key_filename = self.settings['clusters'][cluster]["private_key_file"],
                look_for_keys = True,
                timeout = 10
            )
            sftpclient = client.get_transport().open_sftp_client()
            statinfo = sftpclient.stat(absolute_path)
        except FileNotFoundError as err:
            ret = False
        sftpclient.close()
        client.close()
        return ret
                
    def parse_rsync_output(self, cluster, push_stdout, push_stderr, pull_stdout, pull_stderr, local_path):
        import re
        conn_closed = "connection unexpectedly closed"
        not_exist = "No such file or directory"
        not_supported = 'requested action not supported'
        #not_exist_prog = re.compile(r'change_dir "(.*)" failed: No such file or directory')
        not_exist_prog = re.compile(self.not_exist_format % r'(.*)')
        def print_stderr():
            print('push_stderr: %s' % push_stderr)
            print('pull_stderr: %s' % pull_stderr)
        if(push_stderr.find(not_exist) >= 0 or pull_stderr.find(not_exist) >= 0):
            ret = set()
            for stderr in (push_stderr, pull_stderr):
                m = not_exist_prog.search(stderr)
                if(m):
                    nonexisting_path = m.group(1)
                    if(nonexisting_path.startswith(self.settings['clusters'][cluster]["home"])):
                        ret.add("remotely")
                    elif(nonexisting_path.startswith(local_path)):
                        ret.add("locally")
            #print_stderr()
            math_not_exist = u"\u2204"
            #math_not_exist =  "does not exist "
            return math_not_exist+' '+(" and ".join(ret))

        if(push_stderr.find(not_supported) >= 0 or pull_stderr.find(not_supported) >= 0):
            print_stderr()
            return not_supported

        if(push_stderr.find(conn_closed) >= 0 or pull_stderr.find(conn_closed) >= 0):
            print_stderr()
            return conn_closed

        FILENAME_COLUMN=1
        INFO_COLUMN=0
        upright_arrow = u"\u2197"
        downright_arrow = u"\u2198"
        circ_anticlk = u"\u21ba"
        circ_clk = u"\u21bb"
        hook_left_arrow = u"\u21a9"
        hook_right_arrow = u"\u21aa"

        description = ""
        push_descr = ''
        #if(True or push_stderr == ''):
        if(push_stdout != ''):
            lines = [line.split(maxsplit=1) for line in push_stdout.split('\n') if len(line) > 0]
            #print("push:" + str(lines))
            sent_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('>')]
            recv_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('<')]
            etc_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('*')]
            create_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('c')]
            descr = []
            if(len(recv_files) > 0):
                descr.append("<%d" % len(recv_files))
            if(len(sent_files) > 0):
                descr.append(">%d" % len(sent_files))
            if(len(etc_files) > 0):
                descr.append("*%d" % len(etc_files))
            if(len(create_files) > 0):
                descr.append("c%d" % len(create_files))
            #symbol = 'push'
            symbol = upright_arrow
            push_descr = (len(descr) > 0 and symbol+' ' or '') + ' '.join(descr)
        print(push_stderr)

        pull_descr = ''
        #if(True or pull_stderr == ''):
        if(pull_stdout != ''):
            lines = [line.split(maxsplit=1) for line in pull_stdout.split('\n') if len(line) > 0]
            #print("pull:" + str(lines))
            sent_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('>')]
            recv_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('<')]
            etc_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('*')]
            create_files = [line[FILENAME_COLUMN] for line in lines if line[INFO_COLUMN].startswith('c')]
            descr = []
            if(len(recv_files) > 0):
                descr.append("<%d" % len(recv_files))
            if(len(sent_files) > 0):
                descr.append(">%d" % len(sent_files))
            if(len(etc_files) > 0):
                descr.append("*%d" % len(etc_files))
            if(len(create_files) > 0):
                descr.append("c%d" % len(create_files))
            #symbol = 'pull'
            symbol = downright_arrow
            pull_descr = (len(descr) > 0 and symbol+' ' or '') + ' '.join(descr)
        print(pull_stderr)

        ret = push_descr
        if(len(push_descr) > 0 and len(pull_descr) > 0):
            ret += ','
        ret += pull_descr
        # 
        if(len(ret) == 0):
            smiley = u"\u0263a"
            check_mark = u"\u2713"
            ret = check_mark
        return ret

    def get_corresponding_path(self, item):
        if(len(self.tree.get_children(item)) != 0):
            # this is not a leaf node
            return None
        else:
            # this is a leaf node. find all parents and construct a path
            path = self.tree.item(item,option='text')
            parent_item = self.tree.parent(item)
            while(parent_item != ''):
                path = os.path.join(self.tree.item(parent_item,option='text'), path)
                parent_item = self.tree.parent(parent_item)
            return path
    
    def rebuild_tree_visit_project_folder(self, parent_item, parent_path):
        import re
        run_folder_re = re.compile(r"^[0-9]+(?:\.\w+)?$")

        for f in os.listdir(parent_path):
            fpath = os.path.join(parent_path,f)
            if(os.path.isdir(fpath)):
                if(run_folder_re.match(f)):
                    self.tree.insert(parent_item,"end", text=f)
                else:
                    item = self.tree.insert(parent_item,"end", text=f)
                    self.rebuild_tree_visit_project_folder(item, fpath)
    
    def rebuild_tree(self):
        # remove all
        for item in self.tree.get_children():
            self.tree.delete(item) 

        self.rebuild_tree_visit_project_folder("", self.settings['folders']['setup']['path'])
        
