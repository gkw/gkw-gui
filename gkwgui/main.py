import tkinter as tk
import tkinter.ttk as ttk

import gkwgui.labbook
import gkwgui.clusters
import gkwgui.codes
import gkwgui.projects
import gkwgui.jobs
import gkwgui.data
import gkwgui.gkwdata
import gkwgui.plots
import gkwgui.documents
import gkwgui.eyecandy
import gkwgui.settings
import gkwgui.credits
import gkwgui.news
import gkwgui.help

import json
import jsmin
import os
import io

import spur

class Main(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        settings_filename = os.environ['HOME']+'/.gkwgui.json'
        
        try:
            with open(settings_filename) as settings_file:
                # employ jsmin to allow for comments in the json file.
                # comments are not part of the json specs, but are convenient, so lets allow them
                json_with_comments_stripped = jsmin.jsmin(settings_file.read())
                self.settings = json.load(io.StringIO(json_with_comments_stripped))
                print("The JSON settings file %s has been read successfully" % settings_filename)
        except FileNotFoundError as err:
            print("The JSON settings file %s could not be read, will create new." % settings_filename)
            print(err)
            self.settings = {};
            json.dump(self.settings, settings_file)

        try:
            with open(self.settings['database'],"r+") as db_file:
                self.db = json.load(db_file)
                print("The JSON Database file %s has been read successfully." % self.settings['database'])
        except Exception as err:
            print("The JSON Database file %s could not be read, will create new." % self.settings['database'])
            with open(self.settings['database'],"x+") as db_file:
                self.db = {}
                json.dump(self.db, db_file)

        self.remoteshells = {}
        for cluster in self.settings["clusters"]:
            print(cluster)
            print(self.settings["clusters"][cluster]["private_key_file"])
            
            print("Connecting to %s@%s..." % (self.settings["clusters"][cluster]["username"],self.settings["clusters"][cluster]["hostname"]))
            self.remoteshells[cluster] = spur.SshShell(
                hostname=self.settings["clusters"][cluster]["hostname"],
                username=self.settings["clusters"][cluster]["username"],
                private_key_file=self.settings["clusters"][cluster]["private_key_file"]
            )
        
        self.grid(sticky=tk.N+tk.S+tk.E+tk.W)                       
        self.createWidgets()
        #self.master.protocol("WM_DELETE_WINDOW", handle_wm_delete_window)
        self.master.title('GKW-GUI: Gyrokinetic Workshop Graphical User Interface')

    def handle_wm_delete_window(self):
        print("Window is deleted now.")
        self.quit()
        

    def createWidgets(self):
        notebook = ttk.Notebook(self)
        # make row and column 0 of the top level window's grid stretchable
        top=self.winfo_toplevel()
        top.rowconfigure(0, weight=1)
        top.columnconfigure(0, weight=1)
        # make row and column 0 of the Application widget's grid stretchable
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        
        
        # f = gkwgui.labbook.LabbookFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        f = gkwgui.clusters.ClustersFrame(notebook, self.settings)
        notebook.add(f, text=f.label)
        
        # f = gkwgui.codes.CodesFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)

        # f = gkwgui.projects.ProjectsFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        f = gkwgui.jobs.JobsFrame(notebook, self.settings)
        notebook.add(f, text=f.label)
        
        f = gkwgui.data.DataFrame(notebook, self.settings, self.remoteshells)
        notebook.add(f, text=f.label)

        f = gkwgui.gkwdata.GKWDataFrame(notebook, self.settings, self.remoteshells)
        notebook.add(f, text=f.label)
        
        f = gkwgui.plots.PlotsFrame(notebook, self.settings)
        notebook.add(f, text=f.label)
        
        # f = gkwgui.documents.DocumentsFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        # f = gkwgui.eyecandy.EyecandyFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        # f = gkwgui.settings.SettingsFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        # f = gkwgui.credits.CreditsFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        # f = gkwgui.news.NewsFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        # f = gkwgui.help.HelpFrame(notebook, self.settings)
        # notebook.add(f, text=f.label)
        
        notebook.grid(row=0, column=0,sticky=tk.N+tk.S+tk.E+tk.W)

    # def test_function(self):
    #     tkMessageBox.showwarning(
    #         "File could not be opened",
    #         "I cannot open this file. So sorry."
    #     )
    #     file_path_string = tkinter.filedialog.askopenfilename()
