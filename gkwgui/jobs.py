import tkinter as tk
import tkinter.ttk as ttk

#SQLite is a C library that provides a lightweight disk-based database
#that doesn't require a separate server process and allows accessing
#the database using a nonstandard variant of the SQL query language.
#The sqlite3 module was written by Gerhard Häring. It provides a SQL
#interface compliant with the DB-API 2.0 specification described by
#PEP 249.
import sqlite3

import os
import datetime

try:
    from collections import OrderedDict
except ImportError:
    from .utils import OrderedDict

class JobsFrame(ttk.Frame):

    label = 'Jobs'

    jobname_sha_table = {}
    
    def __init__(self, master, settings):
        super().__init__()
        
        self.settings = settings

        self.buttonsframe = tk.Frame(self)
        self.refresh_button = ttk.Button(self.buttonsframe, text='refresh tree', command=self.rebuild_tree)
        self.refresh_button.pack(side='left')


        label = tk.Label(self.buttonsframe, text="Consider last ")
        label.pack(side='left')

        # associate a variable to a spinbox. 
        self.deltat_spinbox_value = tk.StringVar()
        self.deltat_spinbox_value.set(str(settings["jobs"]["default_deltat"]))
        self.deltat_spinbox_value.trace('w',self.update_deltat)
        deltat_spinbox = tk.Spinbox(self.buttonsframe, from_=1, to=90, increment=1, width=4, textvariable=self.deltat_spinbox_value)
        deltat_spinbox.pack(side='left')

        label = tk.Label(self.buttonsframe, text=" days.")
        label.pack(side='left')
        
        
        self.buttonsframe.grid(column=0,row=0,sticky=tk.W)
        
        self.tree = ttk.Treeview(self)

        self.job_properties = OrderedDict()
        self.job_properties["jobname"] = [600,"jobname"]
        self.job_properties["output_path"] = [100,"output path"]
        self.job_properties["nodes"] = [10,"nodes"]
        self.job_properties["tasks"] = [10,"tasks"]
        self.job_properties["req_time"] = [10,"req. time"]
        self.job_properties["status"] = [10,"status"]
        self.job_properties["last_seen"] = [10,"last seen"]
        self.job_properties["run_time"] = [10,"run time"]
    
        self.tree["columns"]=[key+"col" for key in self.job_properties.keys()]
        COLUMN_WIDTH=0
        COLUMN_NAME=1
        for key,val in self.job_properties.items():
            self.tree.column(key+"col", width=val[COLUMN_WIDTH])
            self.tree.heading(key+"col", text=val[COLUMN_NAME])
        
        # make that cell of the grid stretchable
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        self.tree.grid(column=0,row=1,sticky=tk.N+tk.S+tk.E+tk.W)

        self.rebuild_tree()

    def update_deltat(self, var_obj, b, trace_mode):
        try:
            self.rebuild_tree()
        except:
            # ignore the error which occurs when the treeview does not yet exist
            pass
        
    def rebuild_tree(self):
        
        # remove all
        for item in self.tree.get_children():
            self.tree.delete(item) 

        # get the full path to this Python file here
        dir_path = os.path.dirname(os.path.realpath(__file__))
        data_path = os.path.join(dir_path,'../data')

        deltat_recently = datetime.timedelta(days=int(self.deltat_spinbox_value.get()))
        
        root_parent_item = ""
        for cluster in self.settings["clusters"]:
        
            cluster_parent_item = self.tree.insert(root_parent_item,"end", text=cluster)
            try:
                
                database_file = os.path.join(data_path,self.settings["clusters"][cluster]["database"])
                print("Reading database %s" % database_file)
                conn = sqlite3.connect(database_file, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
                #Rows wrapped with this class can be accessed both by index (like
                #tuples) and case-insensitively by name:
                conn.row_factory = sqlite3.Row
                
                #Once you have a Connection, you can create a Cursor object and call
                #its execute() method to perform SQL commands:
                c = conn.cursor()

                # get the data from the db:
                # recently finished runs
                now = datetime.datetime.now()

                c.execute('SELECT * FROM jobs_stats WHERE user=? AND status=? AND last_seen>? ORDER BY jobid ASC', (self.settings["clusters"][cluster]["username"],'F',now-deltat_recently))

                table_updated = False
                for row in c:
                    if(not table_updated):
                        #only update if the query has found rows
                        self.update_jobname_sha_table(cluster)
                        table_updated = True

                    values = [k == 'jobname' and self.translate_sha(cluster, row[k]) or row[k]
                              for k in self.job_properties.keys()]
                    self.tree.insert(cluster_parent_item,"end", text=row['jobid'],
                                     values=values)

                # the unfinished runs
                for row in c.execute('SELECT * FROM jobs_stats WHERE user=? AND status!=?', (self.settings["clusters"][cluster]["username"],'F')):
                    values = [k == 'jobname' and self.translate_sha(cluster, row[k]) or row[k]
                              for k in self.job_properties.keys()]
                    self.tree.insert(cluster_parent_item,"end", text=row['jobid'],
                                     values=values)

                # We can also close the connection if we are done with it.
                conn.close()
            except sqlite3.OperationalError as err:
                print("Could not access database %s" % database_file)
                print(err)


    def update_jobname_sha_table(self, cluster):
        try:
            if(cluster not in self.jobname_sha_table):
                self.jobname_sha_table[cluster] = {}

            import spur
            import re
            import signal
            localshell = spur.LocalShell()
            # Use a private key

            user_at_host = "%s@%s" %(self.settings["clusters"][cluster]["username"],self.settings["clusters"][cluster]["hostname"])
            print("Connecting to %s..." % user_at_host)
            def timeout_fail_handler(signum, frame):
                raise Exception("Timeout, connection to %s could not be established quickly enough." % user_at_host)
            def timeout_ignore_handler(signum, frame):
                pass
            #register the signal handler
            signal.signal(signal.SIGALRM, timeout_fail_handler)

            #start the timer
            timeout_seconds = 10
            signal.alarm(timeout_seconds)

            remoteshell = spur.SshShell(
                hostname=self.settings["clusters"][cluster]["hostname"],
                username=self.settings["clusters"][cluster]["username"],
                private_key_file=self.settings["clusters"][cluster]["private_key_file"],
            )
            
            with remoteshell.open(self.settings["clusters"][cluster]["home"]+"/jobnames.sha", "r") as remote_file:
                print("Reading remote file content...")

                line_prog = re.compile(r'^\s*([^\s]+)\s+([^\s]+)\s+(.+)$')
                lines = remote_file.readlines()
                for line in lines:
                    #print(line.strip())
                    m = line_prog.match(line.strip())
                    if(m):
                        sha = m.group(1)
                        plainname = m.group(2)
                        self.jobname_sha_table[cluster][sha] = plainname
                    else:
                        print("no match when parsing sha table (this should never happen!): '%s'" % line.strip())

        except Exception as err:
            print(err)
        finally:
            #register the signal handler which will just ignore the alarm
            signal.signal(signal.SIGALRM, timeout_ignore_handler)
            #replace the pending SIGALRM 
            signal.alarm(0)

    def translate_sha(self, clustername, jobname):
        try:
            sha = jobname.replace('GKW-','')
            unshortened_sha = self.find_key(self.jobname_sha_table[clustername], sha)
            return self.jobname_sha_table[clustername][unshortened_sha]
        except:
            print("could not translate %s %s" % (clustername, jobname))
            return jobname

    def find_key(self, d, key_start):
        """
        This function is a workaround: some data was collected into the jobs_stats database without using
        the -w (wide columns) argument for qstat. As a consequence the jobnames got truncated.
        Given the truncated sha, this function tries to find the full one.
        """
        for k in d.keys():
            if(k.startswith(key_start)):
                return k
        return key_start
