import tkinter as tk
import tkinter.ttk as ttk

class ProjectsFrame(ttk.Frame):

    label = 'Projects'
    
    def __init__(self, settings, master=None):
        super().__init__()
        
        self.settings = settings
