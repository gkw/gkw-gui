import abc

class DictWithGetters(dict):
    class Getter:
        def __init__(self,value):
            self.value = value
        def get(self):
            return self.value
        def set(self, value):
            self.value = value
        
    def __init__(self, orig_dict):
        super().__init__(zip(orig_dict.keys(), [self.Getter(v) for v in orig_dict.values()]))

class Plottable(abc.ABC):
    """
    Derive your plot from this base class to make GKW-GUI enlist it automatically.

    Please see the examples in the python/ subfolder of the GKW repository.

    """

    def __init__(self, h5filename, parse_cmd_line_args=False):
        import argparse
        self.h5filename = h5filename
        self.parser = argparse.ArgumentParser(add_help=False)

        self.parser.add_argument("--title", help="title",
                                 default=self.getDefaultTitle(),
                                 type=str)
        
        self.specifyArguments(self.parser)
        if(parse_cmd_line_args):
            # parse arguments from command line.  To be able to obtain
            # the value via self.arguments["parametername"].get() it
            # is wrapped into a DictWithGetters object.
            self.arguments = DictWithGetters(vars(self.parser.parse_known_args()[0]))
        else:
            # add tk Variable objects later, which track values of
            # corresponding tk widgets.
            self.arguments = {}

    def __getstate__(self):
        # Copy the object's state from self.__dict__ which contains
        # all our instance attributes. Always use the dict.copy()
        # method to avoid modifying the original state.
        state = {}
        state["h5filename"] = self.h5filename
        state["arguments"] = {key:value.get() for key, value in self.arguments.items()}
        return state

    def __setstate__(self, state):

        self.h5filename = state["h5filename"]
        self._raw_unpickled_arguments = state["arguments"].items()
        self.arguments = None
        
        # # Restore instance attributes (i.e., filename and lineno).
        # self.__dict__.update(state)
        # # Restore the previously opened file's state. To do so, we need to
        # # reopen it and read from it until the line count is restored.
        # file = open(self.filename)
        # for _ in range(self.lineno):
        #     file.readline()
        # # Finally, save the file.
        # self.file = file
        

    def specifyArguments(self, parser):
        """
        Make some Tkinter widgets to control parameters of the plot.
        This is optional.

        Please see the source code at %s for some examples.

        """ % __file__
        # self.parser.add_argument("--fooInt", help="some integer parameter (0-based)",
        #                          default=5,
        #                          type=int)
        # self.parser.add_argument("--fooFloat", help="some float parameter",
        #                          default=1.23456e-7,
        #                          type=float)
        # self.parser.add_argument("--fooBool", help="some boolean parameter",
        #                          default=False,
        #                          #action="store_true",
        #                          type=bool)
        # self.parser.add_argument("--fooOptions", help="some integer options",
        #                          default=1,
        #                          type=int, choices=[0, 1, 2])
        # self.parser.add_argument("--fooArbOptions", help="some arbitrary options",
        #                          default="all",
        #                          choices=["some", 1.23, "many", "all"])
        # self.parser.add_argument("--fooArbOptions2", help="some arbitrary options",
        #                          default="all",
        #                          choices=["some", "two", "many", "all"])
                                 

    @classmethod
    @abc.abstractmethod
    def isPlottable(cls,h5filename):
        """
        Return True if the data in the given HDF5 file is sufficient to draw the plot.
        """
        return True

    @abc.abstractmethod
    def plot(self, ax):
        """
         ax: a matplotlib axes object

        """
        pass
        
    def getDefaultTitle(self):
        """
         Subclasses are supposed to override this method, to specify a default title.

         Return a string, the default title of this plot.
        """
        return ""

    def _plotAndSetTitle(self, ax):
        self.plot(ax)

        def joinTitles(s1, s2, delim=", "):
            """
            Join two strings. If both are not empty then put the delim string between them.
            """
            s1 = s1.strip()
            s2 = s2.strip()
            if(len(s1) == 0 or len(s2) == 0):
                return s1 + s2
            else:
                return s1 + delim + s2

        ax.set(title=joinTitles(ax.properties()['title'], self.arguments['title'].get()))

    def export(self):
        """
        Return the data of the plot as a numpy array object.
        This can then be written to a matlab file by the GKW GUI, using scipy.io.savemat()
        """
        return None
        
    def _putTkWidgets(self, master, auto_redraw_callback):
        """
        Given the ArgumentParser Arguments defined in specifyArguments(),
        this method created Tkinter widgets.
        """
        import tkinter as tk
        import tkinter.ttk as ttk
        groupid = 0
        # we consider *only* the optional arguments. These appear as action group 1
        g = self.parser._action_groups[1]

        # for each action create a suitable Tkinter widget
        for a in g._actions:
            if(a.type is not bool):
                # the checkbutton will bring its own label
                tk.Label(master, text=a.help+": ("+a.dest+")", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
            if(a.choices):
                choices = a.choices
                if(a.type is int):
                    self.arguments[a.dest] = tk.IntVar()
                elif(a.type is float):
                    self.arguments[a.dest] = tk.DoubleVar()
                elif(a.type is bool):
                    self.arguments[a.dest] = tk.BooleanVar()
                else:
                    self.arguments[a.dest] = tk.StringVar()
                    choices = [str(elem) for elem in choices]

                combo = ttk.Combobox(master,
                                     width=20,
                                     values=choices,
                                     textvariable=self.arguments[a.dest],
                                     state="readonly")
                if(a.default in choices):
                    combo.current(choices.index(a.default))
                else:
                    combo.current(0)
                combo.pack(side=tk.TOP, fill=tk.X, expand=False)
            elif a.type is bool:
                self.arguments[a.dest] = tk.BooleanVar()
                if(a.default):
                    self.arguments[a.dest].set(a.default)
                tk.Checkbutton(master,
                               text=a.help+" ("+a.dest+")",
                               variable=self.arguments[a.dest],
                               offvalue=False,
                               onvalue=True,
                               anchor=tk.W,
                               justify=tk.LEFT).pack(side=tk.TOP, fill=tk.X, expand=False)

            elif a.type is int:
                self.arguments[a.dest] = tk.IntVar()
                if(a.default):
                    self.arguments[a.dest].set(a.default)
                tk.Spinbox(master,
                           textvariable=self.arguments[a.dest],
                           width=4,
                           from_=-2**16, to=2**16, increment=1).pack(side=tk.TOP, fill=tk.X, expand=False)

            elif False:
                # slide = tk.Scale(parent,
                #     orient=tk.HORIZONTAL,
                #     showvalue=0,
                #     variable=tkvar,
                #     from_=0, to=maxval, resolution=res)
                pass
            else:
                # put a simple entry box
                
                if(a.type is int):
                    self.arguments[a.dest] = tk.IntVar()
                elif(a.type is float):
                    self.arguments[a.dest] = tk.DoubleVar()
                elif(a.type is bool):
                    self.arguments[a.dest] = tk.BooleanVar()
                else:
                    self.arguments[a.dest] = tk.StringVar()
                if(a.default):
                    self.arguments[a.dest].set(a.default)
                tk.Entry(master, textvariable=self.arguments[a.dest]).pack(side=tk.TOP, fill=tk.X, expand=False)
                # use this to retrieve the value:
                # self.arguments[a.dest].get()

            self.arguments[a.dest].trace('w',auto_redraw_callback)

                
            


