import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox
from PIL import Image, ImageTk
import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2TkAgg)
import matplotlib.backend_bases

import traceback
import functools

class PlotsFrame(ttk.Frame):

    label = 'Plots'
    
    def __init__(self, master, settings):
        super().__init__()

        self.settings = settings

        self.plot_selection = []

        path = os.path.dirname(__file__)
        yes_icon_path = os.path.join(path,"mate-ax-slowkeys-yes.png")
        no_icon_path = os.path.join(path,"mate-ax-slowkeys-no.png")
        self.yes_icon = ImageTk.PhotoImage(Image.open(yes_icon_path))
        self.no_icon = ImageTk.PhotoImage(Image.open(no_icon_path)) 
        
        self.buttonsframe = tk.Frame(self)
        file_diff_label = tk.Label(self.buttonsframe, text="GKW HDF5 data file:")
        file_diff_label.pack(side='left')
        
        self.h5file_combo_value = tk.StringVar()
        self.h5file_combo_value.trace('w',self.update_h5file_combo)
        self.h5file_combo = ttk.Combobox(self.buttonsframe, width=150,
                                         values=self.read_history(),
                                         textvariable=self.h5file_combo_value)
        
        self.h5file_combo.bind('<Return>', self.h5file_changed)
        self.h5file_combo.pack(side='left')

        self.button = ttk.Button(self.buttonsframe, text='choose', command=self.open_data)
        self.button.pack(side='left')

        self.message_label = tk.Label(self.buttonsframe, text="")
        self.message_label.pack(side='left')

        self.buttonsframe.grid(column=0,row=0,columnspan=99, sticky=tk.W)

        ####################################################################################################
        self.tree = ttk.Treeview(self)
        self.tree.column("#0", width=260)
        self.tree.heading("#0", text="Available plots:")
        
        # make that cell of the grid stretchable
        self.rowconfigure(2, weight=1)
        self.columnconfigure(0, weight=0)
        self.tree.grid(column=0, row=1, rowspan=3, sticky=tk.NSEW)
        scrollbar = tk.Scrollbar(self)
        scrollbar.grid(column=1, row=1, rowspan=3, sticky=tk.NSEW)
        scrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=scrollbar.set)

        ####################################################################################################

        # create a persistent matplotlib figure object
        self.fig = plt.figure(tight_layout=True)
        # we cannot use len(self.fig.axes) because seems to count
        # other objects like colorbars, too
        self.axes_counter = 0

        ####################################################################################################

        self.center_frame = tk.Frame(self)

        button = ttk.Button(self.center_frame, text='<- refresh plots list', command=self.refresh_plots_list)
        button.pack(side=tk.TOP, fill=tk.X, expand=False)
        button = ttk.Button(self.center_frame, text='clear ->', command=self.clear_plots)
        button.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        self.auto_redraw_check_value = tk.BooleanVar()
        tk.Checkbutton(self.center_frame,
                       text='auto\nredraw',
                       variable=self.auto_redraw_check_value,
                       offvalue=False,
                       onvalue=True).pack(side='top', fill=tk.X, expand=False)

        button = ttk.Button(self.center_frame, text='redraw ->', command=self.redraw)
        button.pack(side=tk.TOP, fill=tk.X, expand=False)

        tk.Label(self.center_frame, text="skip n subplots:", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.skip_n_spbox_value = tk.IntVar()
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.skip_n_spbox_value,
                           from_=0, to=10, increment=1)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)

        
        button = ttk.Button(self.center_frame, text='plot into\nnext subplot ->', command=functools.partial(self.put_plots, into_next=True))
        button.pack(side=tk.TOP, fill=tk.X, expand=False)

        button = ttk.Button(self.center_frame, text='use this data\nfor all and redraw ->', command=self.replace_h5filename_and_redraw)
        button.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        tk.Label(self.center_frame, text="Columns:", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.columns_spbox_value = tk.IntVar()
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.columns_spbox_value,
                           from_=1, to=8, increment=1)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)

        tk.Label(self.center_frame, text="Rows:", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.rows_spbox_value = tk.IntVar()
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.rows_spbox_value,
                           from_=1, to=8, increment=1)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        button = ttk.Button(self.center_frame, text='plot into\nlast subplot ->', command=functools.partial(self.put_plots, into_next=False))
        button.pack(side=tk.TOP, fill=tk.X, expand=False)

        button = ttk.Button(self.center_frame, text='remove last subplot ->', command=self.remove_last_axes)
        button.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        tk.Label(self.center_frame, text="Width\n(inches):", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.fig_w_spbox_value = tk.DoubleVar()
        self.fig_w_spbox_value.set(self.fig.get_size_inches()[0])
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.fig_w_spbox_value,
                           from_=0.0, to=999.0, increment=0.1)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)

        tk.Label(self.center_frame, text="Height\n(inches):", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.fig_h_spbox_value = tk.DoubleVar()
        self.fig_h_spbox_value.set(self.fig.get_size_inches()[1])
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.fig_h_spbox_value,
                           from_=0.0, to=999.0, increment=0.1)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)

        tk.Label(self.center_frame, text="DPI:", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.dpi_spbox_value = tk.IntVar()
        self.dpi_spbox_value.set(self.fig.get_dpi())
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.dpi_spbox_value,
                           from_=10, to=1200, increment=10)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        tk.Label(self.center_frame, text="Pixels per dot:", justify=tk.LEFT, anchor=tk.W).pack(side=tk.TOP, fill=tk.X, expand=False)
        self.pix_spbox_value = tk.DoubleVar()
        self.pix_spbox_value.set(1.0)
        spbox = tk.Spinbox(self.center_frame,
                           textvariable=self.pix_spbox_value,
                           from_=0, to=100, increment=0.1)
        spbox.pack(side=tk.TOP, fill=tk.X, expand=False)

        button = ttk.Button(self.center_frame, text='export\nfor\nMATLAB', command=self.export_data)
        button.pack(side=tk.TOP, fill=tk.X, expand=False)
        
        self.center_frame.grid(column=2,row=1,rowspan=3,sticky=tk.NSEW)
        self.columnconfigure(2, weight=0)

        ####################################################################################################
        
        # set up a canvas with scrollbars
        self.canvas = tk.Canvas(self)
        self.canvas.grid(column=3, row=2, sticky=tk.NSEW)
        self.columnconfigure(3, weight=1)
        
        xScrollbar = tk.Scrollbar(self, orient=tk.HORIZONTAL)
        yScrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        
        xScrollbar.grid(row=3, column=3, sticky=tk.EW)
        yScrollbar.grid(row=2, column=4, sticky=tk.NS)
        
        self.canvas.config(xscrollcommand=xScrollbar.set)
        xScrollbar.config(command=self.canvas.xview)
        self.canvas.config(yscrollcommand=yScrollbar.set)
        yScrollbar.config(command=self.canvas.yview)

        #self.fig.set_size_inches(self.fig_w_spbox_value.get(), self.fig_h_spbox_value.get(), forward=True)

        # attach this figure to an object which includes drawing logic
        self.figure_agg = FigureCanvasTkAgg(self.fig, self.canvas)
        self.figure_agg.draw()
        # and this is the actual widget:
        self.plot_canvas = self.figure_agg.get_tk_widget()
        self.plot_canvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        # and connect the plot canvas with the scrolling canvas
        self.canvas_window_item = self.canvas.create_window(0, 0, window=self.plot_canvas, anchor=tk.NW)
  
        # add a toolbar
        toolbar_frame = tk.Frame(self)
        toolbar = NavigationToolbar2TkAgg(self.figure_agg, toolbar_frame)
        toolbar.update()
        #self.figure_agg._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        def on_key_press(event):
            #print("you pressed {}".format(event.key))
            matplotlib.backend_bases.key_press_handler(event, self.figure_agg, toolbar)
        self.figure_agg.mpl_connect("key_press_event", on_key_press)
        toolbar_frame.grid(column=3,row=1,sticky=tk.EW)

        self.arguments_scrolled_window = ScrolledWindow(self)
        self.arguments_scrolled_window.grid(column=5, row=1, rowspan=3, sticky=tk.NSEW)
        
        # self.arguments_scrollbar = tk.Scrollbar(self)
        # self.arguments_scrollbar.grid(column=6, row=1, rowspan=3, sticky=tk.NSEW)
        # self.columnconfigure(6, weight=0)
        
        # self.arguments_canvas = tk.Canvas(self)
        # self.arguments_canvas.grid(column=5, row=1, rowspan=3, sticky=tk.NSEW)
        # self.arguments_canvas.config(yscrollcommand=self.arguments_scrollbar.set)
        # self.arguments_scrollbar.config(command=self.arguments_canvas.yview)

        
        self.put_arguments_frame()
        self.columnconfigure(5, weight=0)




    def put_arguments_frame(self):
        self.arguments_frame = tk.Frame(self.arguments_scrolled_window.scrollwindow)
        self.arguments_frame.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        
        # #self.arguments_frame.grid(column=5, row=1, rowspan=3, sticky=tk.NSEW)
        # self.arguments_canvas.create_window(0, 0, window=self.arguments_frame, anchor=tk.NW)

    
    def auto_redraw_callback(self, var_obj, b, trace_mode):
        if(self.auto_redraw_check_value.get()):
            self.redraw()

    def set_canvas_size(self):
        width_pix, height_pix = [int(i*self.dpi_spbox_value.get()*self.pix_spbox_value.get()) for i in self.fig.get_size_inches()]
        
        self.plot_canvas.config(width=width_pix, height=height_pix)
        self.canvas.itemconfigure(self.canvas_window_item, width=width_pix, height=height_pix)
        self.canvas.config(scrollregion=self.canvas.bbox(tk.ALL))
        #self.canvas.config(scrollregion=canvas.bbox(tk.ALL),width=200,height=200)
        
    def redraw(self):
        try:
            self.fig.clear()
            self.fig.set_dpi(self.dpi_spbox_value.get())
            self.fig.set_size_inches(self.fig_w_spbox_value.get(), self.fig_h_spbox_value.get(), forward=True)

            self.set_canvas_size()
            
            for t in self.plot_selection:
                rows = t[1]
                cols = t[2]
                axes_counter = t[3]
                ax = self.fig.add_subplot(rows,
                                          cols,
                                          axes_counter)
                self.draw_plot(t[0], ax)
            self.figure_agg.draw()
        except Exception as err:
            traceback.print_exc()

    def replace_h5filename_and_redraw(self):
        h5filename = self.h5file_combo_value.get()
        if(h5filename is None or len(h5filename) == 0):
            return

        for t in self.plot_selection:
            t[0].h5filename = h5filename
        self.redraw()

    def clear_plots(self):
        self.fig.clear()
        self.axes_counter = 0
        self.arguments_frame.destroy()
        self.put_arguments_frame()
        self.figure_agg.draw()
        self.plot_selection = []

    def remove_last_axes(self):
        self.fig.delaxes(self.fig.axes[-1])
        self.plot_selection = [elem for elem in self.plot_selection if elem[3] != self.axes_counter]
        self.axes_counter -= 1
        self.figure_agg.draw()
        
    def read_history(self):
        try:
            with open(self.settings["history"]["data_file_history_file"], mode="r") as f:
                ret = [line.strip() for line in f.readlines()]
        except:
            ret = []
        return ret

    def update_history(self):
        try:
            h5filename = self.h5file_combo_value.get()
            new_list = (h5filename,) + self.h5file_combo["values"]
            new_list = list(set(new_list))
            new_list.sort()
            new_list = new_list[:max(len(new_list),10)]
            self.h5file_combo["values"] = new_list
            with open(self.settings["history"]["data_file_history_file"], mode="w") as f:
                f.writelines([line+'\n' for line in self.h5file_combo["values"]])
        except Exception as err:
            print("Could not write history.")
            traceback.print_exc()

    def h5file_changed(self):
        self.update_history()
        self.refresh_plots_list()
    
    def refresh_plots_list(self):
        import pkgutil
        import gkwgui.plottable
        import importlib
        import inspect

        # remove all
        for item in self.tree.get_children():
            self.tree.delete(item) 

        h5filename = self.h5file_combo_value.get()
        if(h5filename is None or len(h5filename) == 0):
            return
        self.plottables = {}
        parent_item = ""
        for module_loader, name, ispkg in pkgutil.iter_modules(
                path=self.settings["plot_repositories"]):
            spec = module_loader.find_spec(name)
            try:
                module = spec.loader.load_module()
            except SyntaxError as err:
                print(name,"could not be loaded (SyntaxError)")
                traceback.print_exc()
                continue
            except RuntimeError as err:
                print(name,"could not be loaded (RuntimeError)")
                traceback.print_exc()
                continue
            
            for k, v in module.__dict__.items():
                # if v is boxes.Boxes:
                #     continue
                if (inspect.isclass(v)
                    and v is not gkwgui.plottable.Plottable
                    and issubclass(v, gkwgui.plottable.Plottable)):
                    plot = v
                    # so v is a class, and indeed a subclass of Plottable.
                    # Then put it into the list:
                    full_name = name+"."+v.__name__
                    try:
                        self.plottables[full_name] = (v, v.isPlottable(h5filename))
                    except NameError as err:
                        traceback.print_exc()
                        print("This error occurred for",full_name)
                    except OSError as err:
                        traceback.print_exc()
                        print("This error occurred for",full_name)
                    #self.tree.insert(parent_item,"end", text=full_name)
                    #generators[modname + '.' + v.__name__] = v

        # make sure they appear in a defined order:
        plotlist = list(self.plottables.keys())
        plotlist.sort()
        for full_name in plotlist:
            self.tree.insert(parent_item,"end", text=full_name,
                             image=self.plottables[full_name][1] and self.yes_icon or self.no_icon)

    def put_plots(self, into_next=True):
        import matplotlib.pyplot as plt

        h5filename = self.h5file_combo_value.get()
        if(h5filename is None or len(h5filename) == 0):
            return

        items = self.tree.selection()
        if(items == ''):
            # update all items
            items = []
            self.message_label.configure(text="Select a run from the project tree first!")
        else:
            self.message_label.configure(text="Drawing the plots, please wait...")

            for item in items:
                # fig = plt.figure(figsize=(5, 4), dpi=100)
                # t = np.arange(0, 3, .01)
                # fig.add_subplot(111).plot(t, 2 * np.sin(2 * np.pi * t))
                itemtext = self.tree.item(item,option='text')
                if(not self.plottables[itemtext][1]):
                    continue

                # so this is the Plottable class for this plot:
                pclass = self.plottables[itemtext][0]
                p = pclass(h5filename)

                try:
                    if(into_next):
                        self.axes_counter += 1 + self.skip_n_spbox_value.get()
                    ax = self.fig.add_subplot(self.rows_spbox_value.get(),
                                              self.columns_spbox_value.get(),
                                              self.axes_counter)
                except Exception as err:
                    traceback.print_exc()
                    self.clear_plots()
                    print("The plot area seems full. Forced clear!")
                    self.axes_counter += 1 + self.skip_n_spbox_value.get()
                    ax = self.fig.add_subplot(self.rows_spbox_value.get(),
                                              self.columns_spbox_value.get(),
                                              self.axes_counter)
                    
                self.plot_selection.append((p,
                                            self.rows_spbox_value.get(), self.columns_spbox_value.get(),
                                            self.axes_counter))

                # reset the skip entry
                self.skip_n_spbox_value.set(0)

                frame = tk.LabelFrame(self.arguments_frame, text=p.__class__.__name__)
                frame.pack(side='top')
                p._putTkWidgets(frame, self.auto_redraw_callback)
                
                self.draw_plot(p, ax)


            if(self.columns_spbox_value.get() > 1 or self.rows_spbox_value.get() > 1):
                pass
                #self.fig.align_labels()
                
                # this is available through a toolbar button, anyway:
                #plt.subplot_tool()
        
            self.figure_agg.draw()
            self.message_label.configure(text="")



    def draw_plot(self, p, ax):
        try:
            p._plotAndSetTitle(ax)
        except Exception as err:
            traceback.print_exc()
            tkinter.messagebox.showerror("Error - could not draw this plot", traceback.format_exc())


    def export_data(self):
        plots = []
        for t in self.plot_selection:
            plot_struct = {}
            exported_data = t[0].export()
            if(exported_data is None):
                tkinter.messagebox.showinfo("Could not export.", "The number %d at subplot (row: %d, column: %d) does not provide export data." % (t[3], t[1], t[2]))
                continue
            data, x, y, desc = exported_data
            if(data is not None):
                plot_struct['data'] = data
                if(x is not None):
                    plot_struct['x'] = x
                if(y is not None):
                    plot_struct['y'] = y
                if(desc is not None):
                    plot_struct['description'] = desc
                else:
                    plot_struct['description'] = 'NA'
                #plot_struct['title'] = self.fig.axes[t[3]].properties()['title']
                plots.append(plot_struct)
        if(len(plots) == 0):
            tkinter.messagebox.showinfo("Could not export.", "None of these plots supports exporting the data.")
            return
        try:
            import tkinter.filedialog
            try:
                h5filename = self.h5file_combo_value.get()
                initialdir = os.path.dirname(h5filename)
            except:
                #this is said to be the filesystem root, but for me it
                #turns out as the working dir
                initialdir = os.path.abspath(os.sep)
                
            filename = tkinter.filedialog.asksaveasfilename(initialdir = initialdir,
                                                            title = "Select file to save data to",
                                                            filetypes = (("Matlab data","*.mat"),("all files","*.*")))
            if(filename is None or len(filename) == 0):
                return
            if(filename.endswith('.mat')):
                import scipy.io
                # write it to a file
                scipy.io.savemat(filename, {"plots":plots})
                tkinter.messagebox.showinfo("Success", "Data of %d plots was written to the file %s." % (len(plots), filename))
            else:
                tkinter.messagebox.showinfo("", "No data was written. Please use a filename with a supported suffix, to indicate the desired file type.")
                return

            
                
        except Exception as err:
            import traceback
            tkinter.messagebox.showerror("Error - could not save this data", traceback.format_exc())
            traceback.print_exc()

        
    def update_h5file_combo(self, var_obj, b, trace_mode):
        self.h5file_changed()
        self.refresh_plots_list()
            
    def open_data(self):
        import tkinter.filedialog
        import os

        try:
            h5filename = self.h5file_combo_value.get()
            initialdir = os.path.dirname(h5filename)
        except:
            #this is said to be the filesystem root, but for me it
            #turns out as the working dir
            initialdir = os.path.abspath(os.sep)
            
        filename = tkinter.filedialog.askopenfilename(initialdir = initialdir,
                                                      title = "Select GKW HDF5 data file (typically gkwdata.h5)",
                                                      filetypes = (("HDF5 data","*.h5"), ("GKW input file","*.dat"), ("GKW input profiles","*.prof"), ("all files","*.*")))
        if(len(filename) == 0):
            return
        self.h5file_combo_value.set(str(filename))


class ScrolledWindow(tk.Frame):
    """
    1. Master widget gets scrollbars and a canvas. Scrollbars are connected 
    to canvas scrollregion.

    2. self.scrollwindow is created and inserted into canvas

    Usage Guideline:
    Assign any widgets as children of <ScrolledWindow instance>.scrollwindow
    to get them inserted into canvas

    __init__(self, master, *args, **kwargs)
    docstring:
    Master = master of scrolled window

    """


    def __init__(self, master, *args, **kwargs):
        """Master = master of scrolled window
        canv_w - width of canvas
        canv_h - height of canvas

       """
        super().__init__(master, *args, **kwargs)

        # creating a scrollbars
        self.yscrlbr = tk.Scrollbar(self)
        self.yscrlbr.grid(column = 1, row = 0, sticky = tk.NS)         
        # creating a canvas
        self.canv = tk.Canvas(self)
        self.canv.config(relief = 'flat',
                         width = 10,
                         heigh = 10, bd = 2)
        # placing a canvas into frame
        self.canv.grid(column = 0, row = 0, sticky = tk.NSEW)
        self.rowconfigure(0,weight=1)
        self.columnconfigure(0,weight=1)
        
        # accociating scrollbar comands to canvas scroling
        self.yscrlbr.config(command = self.canv.yview)

        # creating a frame to insert to canvas
        self.scrollwindow = tk.Frame(self)

        self.canv.create_window(0, 0, window = self.scrollwindow, anchor = tk.NW)

        self.canv.config(
                         yscrollcommand = self.yscrlbr.set,
                     )

        # self.yscrlbr.lift(self.scrollwindow)        
        # self.xscrlbr.lift(self.scrollwindow)
        self.scrollwindow.bind('<Configure>', self._configure_window)  
        self.scrollwindow.bind('<Enter>', self._bound_to_mousewheel)
        self.scrollwindow.bind('<Leave>', self._unbound_to_mousewheel)

    def _bound_to_mousewheel(self, event):
        self.canv.bind_all("<MouseWheel>", self._on_mousewheel)   

    def _unbound_to_mousewheel(self, event):
        self.canv.unbind_all("<MouseWheel>") 

    def _on_mousewheel(self, event):
        self.canv.yview_scroll(int(-1*(event.delta/120)), "units")  

    def _configure_window(self, event):
        # update the scrollbars to match the size of the inner frame
        size = (self.scrollwindow.winfo_reqwidth(), self.scrollwindow.winfo_reqheight())
        self.canv.config(scrollregion='0 0 %s %s' % size)
        if self.scrollwindow.winfo_reqwidth() != self.canv.winfo_width():
            # update the canvas's width to fit the inner frame
            self.canv.config(width = self.scrollwindow.winfo_reqwidth())
        if self.scrollwindow.winfo_reqheight() != self.canv.winfo_height():
            # update the canvas's width to fit the inner frame
            self.canv.config(height = self.scrollwindow.winfo_reqheight())
