import tkinter as tk
import tkinter.ttk as ttk

import logging

class LabbookFrame(ttk.Frame):

    label = 'Labbook'
    
    def __init__(self, master, settings):
        super().__init__()

        self.settings = settings
