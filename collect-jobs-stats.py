#!/usr/bin/env python3

# at the moment, this raw data collector only works for the PBS queuing system

from subprocess import Popen, PIPE, STDOUT
import re
import sqlite3
import datetime
import sys
import os
import math
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("username", help="username for ssh connection")
parser.add_argument("hostname", help="hostname for ssh connection")
parser.add_argument("job_name_snippet")
parser.add_argument("cpus_per_node",
                    default=24,
                    type=int)
parser.add_argument("--database",
                    help="path to the sqlite database file",
                    default=None)
parser.add_argument("--system",
                    choices=["PBS","SLURM"],
                    default="PBS")
parser.add_argument("--partition",
                    help="partition of the cluster",
                    default=None)
args = parser.parse_args()


if(args.database is None):
    args.database = args.hostname+'.jobs.db'

now = datetime.datetime.now()

finger_cache = {}

def finger(user, username=None, host=None):
    from subprocess import Popen, PIPE, STDOUT
    import time

    time.sleep(0.25)
    
    import re
    if(username is None and host is None):
        # obtain by local finger
        p = Popen(['finger',user], stdout=PIPE, stdin=None, stderr=None,universal_newlines=True)
    else:
        # finger on the remote host
        p = Popen(['ssh', username+'@'+host,'finger',user], stdout=PIPE, stdin=None, stderr=None,universal_newlines=True)
        print("fingering on %s for user %s..." % (host,user))
            
    data = p.communicate()[0]
    if(re.search('no such user',data)):
        if(username is None and host is None):
            return (user, 'unknown')
        else:
            # finger locally
            return finger(user)
    else:
        # try to parse real name and workinggroup from finger output
        
        match_object = re.search('Name: ([^\n]*)',data)
        if(match_object):
            realname = match_object.group(1)
        else:
            realname = user

        match_object = re.search('Directory: /home/([^/]*)/'+user,data)
        if(match_object):
            workinggroup = match_object.group(1)
        else:
            workinggroup = 'unknown'

        return (realname, workinggroup)


if(args.system == "SLURM"):
    list_queue_command = ['squeue','-o',"'%all'"]
    if(args.partition is not None):
        list_queue_command += ["--partition", args.partition]
elif(args.system == "PBS"):
    list_queue_command = ['qstat','-f']
    
p = Popen(['ssh',args.username+'@'+args.hostname,]+list_queue_command, stdout=PIPE, stdin=PIPE, stderr=PIPE)

list_queue_output = p.communicate()[0].decode('latin1')
list_queue_output = re.split('Job Id:',list_queue_output)
list_queue_output = [paragraph.split('\n') for paragraph in list_queue_output]

#SQLite is a C library that provides a lightweight disk-based database
#that doesn't require a separate server process and allows accessing
#the database using a nonstandard variant of the SQL query language.
#The sqlite3 module was written by Gerhard Häring. It provides a SQL
#interface compliant with the DB-API 2.0 specification described by
#PEP 249.
conn = sqlite3.connect(args.database, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES,timeout=60.0)
#Rows wrapped with this class can be accessed both by index (like
#tuples) and case-insensitively by name:
conn.row_factory = sqlite3.Row

#Once you have a Connection, you can create a Cursor object and call
#its execute() method to perform SQL commands:
c = conn.cursor()


try:
    c.execute('''PRAGMA JOURNAL_MODE=MEMORY''')
    c.execute('''PRAGMA SYNCHRONOUS=NORMAL''')
    c.execute('''PRAGMA LOCKING_MODE=EXCLUSIVE''')
except:
    pass

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS jobs_stats
(jobid varchar unique PRIMARY KEY,user text,queue text,jobname text,sessionid integer,
nodes integer,tasks integer,memory text,req_time text,status text,run_time text,
first_seen timestamp, last_seen timestamp, first_seen_H timestamp, first_seen_R timestamp, allocated_cpus text, output_path text)''')

# create index, otherwise plot will be extremely slow
c.execute('''CREATE INDEX IF NOT EXISTS idx_jobs_stats ON jobs_stats (user, first_seen_R, last_seen)''')


#c.execute('''DROP TABLE user_realnames''')
c.execute('''CREATE TABLE IF NOT EXISTS user_realnames
(user text unique PRIMARY KEY,realname text)''')


try:
    newcol = 'output_path'
    colnames = c.execute('SELECT * FROM jobs_stats').fetchone().keys()
    #print(colnames)
    if(newcol not in colnames):
        print(newcol+" is not in table! add it now.")
        c.execute('ALTER TABLE jobs_stats ADD COLUMN '+newcol+' text')
except:
    pass

if(args.system == "PBS"):
    pass
else:
    import csv
    list_queue_output = csv.reader(list_queue_output[0], dialect='excel', delimiter="|")
    for row in list_queue_output:
        slurm_header = [elem.lower() for elem in row]
        break
jobid = None
#list_queue_output_iterator = iter(list_queue_output)
#list_queue_output_elem = next(list_queue_output_iterator,None)
# qstat_wn_output_iterator = iter(qstat_wn_output)
# qstat_wn_output_elem = next(qstat_wn_output_iterator,None)

# for parsing PBS:
property_continued_prog = re.compile(r'^	(.*)$')

def get_property(paragraph, key):
    prog = re.compile(r'^(\s+)%s = (.*)$' % key)
    val = None
    for line in paragraph:
        m = prog.match(line)
        m_cont = property_continued_prog.match(line)
        if(val is None and m):
            val = m.group(2)
        elif(val is not None and m_cont):
            val += m_cont.group(1)
        elif(val is not None):
            break

    if(val is None):
        return val
    else:
        return val.strip()


only_path_prog = re.compile(r'^[^/]*(/.*)$')

queue_prog = re.compile('[0-9]+\[?\]?\.'+args.job_name_snippet+'[^ ]*')
cpulist_prog = re.compile('^(\+?([rncs][0-9]+)+/[0-9]+\*?[0-9]*)+$')
# Note that on marconi, I have seen jobs like this:
#547960[].r000u1 spilati0 prod     a06           --    1  18   16gb 09:59 B   --

strip_behind_at_prog = re.compile('@.*$')

for job in list_queue_output:
    if(len(job) == 0):
        continue
    if(args.system == "PBS"):
        if(job[0] == ''):
            continue
    elif(args.system == "SLURM"):
        if(job[0] == "ACCOUNT"):
            continue

    if(args.system == "PBS"):
        jobid = job[0].replace('Job Id:','').strip()

        # for prop in ["Resource_List.nodect","Resource_List.mpiprocs","job_state"]:
        #     print("%s = %s" % (prop, get_property(job, prop)))

        user = get_property(job, "Job_Owner")
        #mnobile0@r000u08l03.marconi.cineca.it
        user = strip_behind_at_prog.sub('',user)
    elif(args.system == "SLURM"):
        jobid = job[slurm_header.index("jobid")]
        user = job[slurm_header.index("user")]
    # print(jobid)
    # print(user)

    result = c.execute("SELECT realname FROM user_realnames WHERE user=?", (user,)).fetchone()
    
    if(not result or result["realname"] == user and user not in finger_cache):
        realname = finger(user,args.username,args.hostname)[0]
        finger_cache[user] = realname
        print(user + " is " + realname)
        try:
            c.execute("INSERT INTO user_realnames VALUES (?,?)",(user, realname))
        except:
            c.execute("UPDATE user_realnames SET realname=? WHERE user=?",(realname,user))

    if(args.system == "PBS"):
        queue = get_property(job, "queue")
        jobname = get_property(job, "Job_Name")
        sessionid = get_property(job, "session_id")
    elif(args.system == "SLURM"):
        queue = job[slurm_header.index("partition")]
        jobname = job[slurm_header.index("name")]
        sessionid = None

    # print(queue)
    # print(jobname)

    if(args.system == "PBS"):
        try:
            nodes_str = get_property(job, "Resource_List.nodect")
            nodes = int(nodes_str)
        except:
            try:
                cpus_str = get_property(job, "Resource_List.ncpus")
                nodes = math.ceil(int(cpus_str)/args.cpus_per_node)
            except:
                # I am not able to determine a reasonable node count
                nodes = -1
        try:
            tasks = int(get_property(job, "Resource_List.mpiprocs"))
            #except ValueError, TypeError:
        except Exception as err:
            #print("\n".join(job))
            #print(err)
            # because one can specify '--' instead of an exact number of nodes
            tasks = args.cpus_per_node
            #exit(1)
    elif(args.system == "SLURM"):
        nodes = int(job[slurm_header.index("nodes")])
        #FIXME, this is probably not the thing we want:
        tasks = int(job[slurm_header.index("cpus")])

    if(args.system == "PBS"):
        memory = get_property(job, "Resource_List.mem")
        req_time = get_property(job, "Resource_List.walltime")
        status = get_property(job, "job_state")
        run_time = get_property(job, "resources_used.walltime")
        output_path = get_property(job, "Output_Path")
        output_path = only_path_prog.match(output_path).group(1)
        
        allocated_cpus = get_property(job, "exec_host")
    elif(args.system == "SLURM"):
        memory = job[slurm_header.index("min_memory")]
        req_time = job[slurm_header.index("time_limit")]
        status = job[slurm_header.index("st")]
        run_time = job[slurm_header.index("time")]
        output_path = job[slurm_header.index("work_dir")]

        allocated_cpus = get_property(job, "cpus")
        
            
    try:
        seen_R_timestamp = (status == 'R' and now or None)
        seen_H_timestamp = (status == 'H' and now or None)
        # try to insert this as a new record
        dataline = (jobid,user,queue,jobname,sessionid,nodes,tasks,memory,req_time,status,run_time,
                now,now,seen_H_timestamp,seen_R_timestamp,None,output_path)
        #print(dataline)
        # put sufficiently many question marks into the query
        param_subst_string = ','.join(['?' for elem in dataline])
        command = "INSERT INTO jobs_stats VALUES ("+param_subst_string+")"
        c.execute(command,dataline)
        
    except sqlite3.IntegrityError:
        # the record already exists. So update it.
        
        # first
        
        # get the data from the db
        job_row = c.execute('SELECT * FROM jobs_stats WHERE jobid=?', (jobid,)).fetchone()
        if(job_row["status"] != 'R' and status == 'R'):
            # job has started since last seen
            c.execute("UPDATE jobs_stats SET first_seen_R=? WHERE jobid=?",(now,jobid))
            # additionally, record the cpus which this job has
            # allocated.  This can be considered, to find the
            # exact amount of nodes per user, in cases where
            # the system puts several jobs onto one node.
            
        if(job_row["status"] != 'H' and status == 'H'):
            # job has transited to 'on hold' status since last seen
            c.execute("UPDATE jobs_stats SET first_seen_H=? WHERE jobid=?",(now,jobid))

    #print("update...")
    c.execute("UPDATE jobs_stats SET jobname=?,sessionid=?,nodes=?, tasks=?,memory=?,req_time=?,status=?,run_time=?,last_seen=?,output_path=? WHERE jobid=?",(jobname,sessionid,nodes,tasks,memory,req_time,status,run_time,now,output_path, jobid))
    c.execute("UPDATE jobs_stats SET allocated_cpus=? WHERE jobid=?",(allocated_cpus,jobid))

#print("mark the jobs that used to run, but do not exist anymore, as done...")
c.execute("UPDATE jobs_stats SET status=? WHERE status!=? AND last_seen!=?",('F','F',now))

#print("commit the changes...")
conn.commit()

# We can also close the connection if we are done with it.
conn.close()

print("collect-jobs-stats.py successfully executed at "+str(now))
