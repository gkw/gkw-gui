#!/usr/bin/env python3

# Avoid an error if matplotlib is run from cron
#http://stackoverflow.com/questions/41814254/qxcbconnection-error-when-python-matplotlib-run-as-cron-job
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from subprocess import Popen, PIPE, STDOUT
import re
import sqlite3
import datetime
import numpy as np
import sys
import os


def finger(user):
    from subprocess import Popen, PIPE, STDOUT
    import re
    result = c.execute("SELECT * FROM user_realnames WHERE user=?", (user,))
    if(result):
        try:
            realname = result.fetchone()["realname"]
        except Exception as err:
            # print("could not obtain realname of %s" % user)
            # print(err)
            realname = user
        workinggroup = 'unknown'
        #print(realname)
    else:
        p = Popen(['finger',user], stdout=PIPE, stdin=PIPE, stderr=PIPE,universal_newlines=True)
        data = p.communicate()[0]
        
        if(re.search('no such user',data)):
            return (user, 'unknown')
        else:
            match_object = re.search('Name: ([^\n]*)',data)
            if(match_object):
                realname = match_object.group(1)
            else:
                realname = user

            match_object = re.search('Directory: /home/([^/]*)/'+user,data)
            if(match_object):
                workinggroup = match_object.group(1)
            else:
                workinggroup = 'unknown'

    return (realname, workinggroup)

now = datetime.datetime.now()

#database_file = '/home/btpp/bt302083/bayreuth-scripts/btcluster/jobs.db'
try:
    database_file = sys.argv[1]
except:
    database_file = 'btrzx3-1.jobs.db'

try:
    output_file = sys.argv[2]
except:
    output_file = database_file+".jobs_history.png"

conn = sqlite3.connect('file:%s?mode=ro' % database_file, uri=True,detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES,timeout=60.0)
#Rows wrapped with this class can be accessed both by index (like
#tuples) and case-insensitively by name:
conn.row_factory = sqlite3.Row

#Once you have a Connection, you can create a Cursor object and call
#its execute() method to perform SQL commands:
c = conn.cursor()

try:
    c.execute('''PRAGMA JOURNAL_MODE=MEMORY''')
    c.execute('''PRAGMA SYNCHRONOUS=NORMAL''')
    c.execute('''PRAGMA LOCKING_MODE=EXCLUSIVE''')
except:
    pass

# c.execute('SELECT * FROM jobs_stats')
# rows = c.fetchall()
# print("%d jobs in the database" % (len(rows)))


########### general information ##########
print("Database: %s" % database_file)

result = c.execute("SELECT COUNT(jobid) FROM jobs_stats").fetchone()[0]
print("%d jobs in the database" % (result))

result = c.execute("SELECT COUNT(jobid) FROM jobs_stats WHERE status=?",('F')).fetchone()[0]
print("%d jobs finished" % (result))

result = c.execute("SELECT COUNT(jobid) FROM jobs_stats WHERE status=?",('Q')).fetchone()[0]
print("%d jobs queued" % (result))

result = c.execute("SELECT COUNT(jobid) FROM jobs_stats WHERE status=?",('H')).fetchone()[0]
print("%d jobs on hold" % (result))

result = c.execute("SELECT COUNT(jobid) FROM jobs_stats WHERE status=?",('R')).fetchone()[0]
print("%d jobs running" % (result))

########## find partition of cluster hogged by user ##########

day = datetime.timedelta(days=1)
plot_time_period = day*2

t0 = now - plot_time_period
#deltat = datetime.timedelta(hours=1)
deltat = datetime.timedelta(minutes=10)

#get total all-time-list of users
users = set()
for row in c.execute("SELECT user FROM jobs_stats"):
    users.add(row["user"])
# convert it to a list, to make it ordered
users = tuple(users)
realnames = [finger(user)[0] for user in users]
workgroups = [finger(user)[1] for user in users]
# print(users)
# print(workgroups)

# generate the timebase:

#time = np.arange(matplotlib.dates.date2num(t0), matplotlib.dates.date2num(now), dtype='datetime64[h]')
time = matplotlib.dates.drange(t0,
                               now,
                               deltat)
#ntime = int((now-t0)/deltat)
ntime = len(time)
#time = np.arange(0,ntime);
nodes_per_user = np.array(np.zeros((len(users),ntime)))


print("get the data living on the timebase...")

#t = t0
#j = 0
for j in range(len(time)):
#    time[j] = j
#    time[j] = t
    t = time[j]
    #print('working on:' + str(t))

    for i in range(len(users)):
        # get the number of nodes this user has hogged at that point of time
        nodes_per_user[i][j] = c.execute("SELECT SUM(nodes) FROM jobs_stats WHERE user=? AND first_seen_R<=? AND ?<last_seen",
                                         (users[i],matplotlib.dates.num2date(t),matplotlib.dates.num2date(t))).fetchone()[0]
        # explain_query_plan = "EXPLAIN QUERY PLAN "
        # print(c.execute(explain_query_plan+
        #                                  "SELECT SUM(nodes) FROM jobs_stats WHERE user=? AND first_seen_R<=? AND ?<last_seen",
        #                                  (users[i],matplotlib.dates.num2date(t),matplotlib.dates.num2date(t))))
        # exit(0)
        
        if(np.isnan(nodes_per_user[i][j])):
            #the stackplot has problems with nan, apparently
            #print("nan occurred for user %s" % users[i])
            nodes_per_user[i][j] = 0
        
        #print(nodes_per_user[i][j])
    #t += deltat
    #j += 1

conn.close()

# print("time:")
# print(time.shape)
# print(time)
# print("nodes_per_user:")
data = nodes_per_user
# for line in data[:]:
#     print(line.shape)
#     print(line)
#print(data.shape)


# sort the data and the labels for most important users to come first

time_integrated_usage = np.sum(data,axis=-1)
sorted_index = np.argsort(time_integrated_usage)[::-1]
data = data[sorted_index]
# I don't know the elegant way to do this:
users = [ users[i] for i in sorted_index]
realnames = [ realnames[i] for i in sorted_index]
workgroups = [ workgroups[i] for i in sorted_index]

try:
    colormap = plt.get_cmap('tab20')
except:
    # Vega20 is deprecated in 2.0
    colormap = plt.get_cmap('Vega20')
# summarize a large number of small users (if there are):
NUMBER_OF_USERS_DISPLAYED = colormap.N - 1

if(len(users) > NUMBER_OF_USERS_DISPLAYED):
    data = np.concatenate((data[:NUMBER_OF_USERS_DISPLAYED,:],
                    np.sum(data[NUMBER_OF_USERS_DISPLAYED:,:],0,keepdims=True)))
    users = users[:NUMBER_OF_USERS_DISPLAYED] + ['others']
    realnames = realnames[:NUMBER_OF_USERS_DISPLAYED] + ['others']
    workgroups = workgroups[:NUMBER_OF_USERS_DISPLAYED] + ['others']

print("plot the data...")
    
years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
days = mdates.DayLocator()  # every day
monthsFmt = mdates.DateFormatter('%M')

fig, ax = plt.subplots()

colors = [ colormap(i) for i in range(colormap.N) ]
ax.stackplot(time, data, labels=realnames,colors=colors)

# print("users:")
# print(len(users))
# print(users)
# print("latest usage data:")
# print(data.shape)
# print(data[:,-1])

#ax.plot(time, np.transpose(data))

# format the ticks
# ax.xaxis.set_major_locator(months)
# ax.xaxis.set_major_formatter(monthsFmt)
# ax.xaxis.set_minor_locator(days)

# ax.set_xlim(t0, now)

# format the coords message box
# def nodes_formatter(x):
#     return '%d nodes' % x
# ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
# ax.format_ydata = nodes_formatter
# ax.grid(True)

# rotates and right aligns the x labels, and moves the bottom of the
# axes up to make room for them
# fig.autofmt_xdate()

#ax.set_ylim(0, 500)
#ax.legend(loc='upper left', frameon=False,borderaxespad=0.)
#ax.legend(bbox_to_anchor=(1.05, 1),loc='upper left', frameon=False,borderaxespad=0.)
lgd = ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.22),
                fancybox=True, shadow=True, ncol=3)
ax.set_title('Nodes per user, '+database_file)
datelocator = matplotlib.dates.AutoDateLocator()
ax.xaxis.set_major_locator(datelocator)
ax.xaxis.set_major_formatter(matplotlib.dates.AutoDateFormatter(datelocator))
fig.autofmt_xdate()

#plt.show()
# Save figure using 72 dots per inch
plt.savefig(output_file,dpi=72,bbox_extra_artists=(lgd,), bbox_inches='tight')


# for debugging: print the complete data table
# whole_table = c.execute("SELECT * FROM jobs_stats").fetchall()
# print("-"*50)
# print("\n".join(["\t".join([str(elem) for elem in row]) for row in whole_table]))

