 GKW GUI - Gyrokinetic Workshop Graphical User Interface
============================================

This is a graphical user interface to assist users in running
simulations with the GKW code and handling result data.

 *Copyright (C) 2017
    S.R. Grosshauser

 WHAT THIS TOOL DOES
============================================

This is a graphical user interface intended to be useful for a small
number of tasks.  Note that I wrote this for my own purposes, so this
will assume the GKW data is in HDF5 format and the runs are organised
like I do it personally. It may be very well that it as absolutely no
use for you!

Main purpose:
 
 1. show aggregated information about a users recent simulation jobs on 
    PBS clusters, finished or unfinished; including their working directiory.

 2. show aggregated information about the status of data synchronisation:
    Where is the most recent/largest block of data? Is it already pulled? Is there an updated
    (but forgotten) input file?
    
 3. Run some external tools on the simulation folders
    
 4. quickly make some very basic plots

 
 REQUIREMENTS
============================================

This tool is written in Python3 and depends on

 - the unix rsync tool
 
 - spur library for python, from https://github.com/mwilliamson/spur.py
 - paramiko library for python, but if you have spur working, you also have paramiko
 - pandas library for python
 - jsmin library for python
 - the Tkinter library, which should have been shipped with your Python
   installation by default
 - Scipy, NumPy and Matplotlib

 INSTALLATION
============================================

Install the prerequisites via your preferred method, e.g. by

    pip3 install --user spur paramiko pandas jsmin scipy matplotlib numpy

Setup cron jobs which regularly update SQLite databases in the data/
folder and creates png figures in the graphs/ folder.
Example:

    cd ~/gkw-gui/data
    nice -n +20 ~/gkw-gui/collect-cluster-stats > /dev/null
    nice -n +20 ~/gkw-gui/collect-jobs-stats.py bt302083 btrzx3-2 'mgmt01\.' 24 btrzx3-1.jobs.db > /dev/null

    cd ~/gkw-gui/;
    nice -n +20 ~/gkw-gui/plot-jobs-stats.py data/btrzx3-1.jobs.db graphs/ &
    nice -n +20 ~/gkw-gui/plot-jobs-stats.py data/btrzx2-1.jobs.db graphs/ &
    
You will need to enter the filenames (assumed to be in the `data/`
folder) of the database in the ~/.gkwgui.json file for each cluster.

You can then just start the GKW GUI tool, e.g. by executing the command

    path/to/gkw-gui/gkw-gui

in your shell.

 LOCAL SETTINGS
============================================

GKW GUI will try to read the file ~/.gkwgui.json
See the example file in the gkw-gui folder to see all the parameters
you need to provide.

Note that you must not provide any passwords, but your login must work
passwordless with key files. Otherwise this tool cannot establish SSH
connections.

 TODO/IDEAS
============================================

 [ ] make code more pythonish generally
 [ ] remove dirty hacks
 [ ] make gui responsive while ssh access is made, maybe using the queue module
 [ ] add more helpful functionality
 
 BUGS
============================================

There are probably quite a few bugs.  Please (see license)
note that you use this software on your own risk.  At the moment GKW
GUI is supposed to only *read* information and not to alter any
simulation data.

If you encounter anything that is a bug or does not work as expected
you are welcome to contact the author or (even better) fix it yourself
and send a pull request.

 LICENSE
============================================

(see the LICENSE file)

This file is part of GKW-GUI.

GKW-GUI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GKW-GUI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GKW-GUI.  If not, see <http://www.gnu.org/licenses/>.
